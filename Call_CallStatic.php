<?php

// require_once 'MagicMethod.php';

class Call_CallStatic
{
    protected $id;
    protected $text;
    protected $meta;

    public function __construct($id, $text, array $meta)
    {
        $this->id = $id;
        $this->text = $text;
        $this->meta = $meta;
    }

    protected function retweet()
    {
        $this->meta['retweets']++;
    }

    protected function favourite()
    {
        $this->meta['favourites']++;
    }

    protected static function downvote()
    {
        echo 'this is static function';
    }

    public function __get($property)
    {
        var_dump($this->$property);
    }

    public function __call($method, $parameters)
    {
        if (in_array($method, array('retweet', 'favourite'))) {
            return call_user_func_array(array($this, $method), $parameters); // goij ham kem theo tham so
        }
    }

    public static function __callStatic($name, $arguments)
    {
        echo "Calling static method '$name' " . "with arguments  '" . implode(', ', $arguments) . "' \n";
    }
}

$test = new Call_CallStatic(id: '1', text: 'text 1', meta: ['retweets' => '12', 'favourites' => '33']);
$test->retweet();
print_r($test->meta);
$test::staticfunction('new content');