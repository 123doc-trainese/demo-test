<?php

class Connection
{
    protected $conn;
    private   $dsn, $username, $password;

    public function __construct($dsn, $username, $password)
    {
        $this->dsn      = $dsn;
        $this->username = $username;
        $this->password = $password;
        $this->connect();
        // return s;
    }

    public function __destruct()
    {
        $this->conn = null;
    }

    public function __get($key)
    {
        if (property_exists($this, $key)) {
            //tiến hành lấy giá trị
            return $this->$key;
        }
    }

    private function connect()
    {
        $this->conn = new PDO($this->dsn, $this->username, $this->password);
    }

    public function __sleep(): array
    {
        return array('dsn', 'username', 'password');
    }
}
$dsn = 'mysql:host=localhost;dbname=QLKH';
$conn = new Connection($dsn, 'root', '12345678');

$a = serialize($conn);
echo $a . "\n";

$sql = "SELECT * FROM GiangVien";
$link = $conn->conn;
$result = $link->query($sql);
$result = $result->fetch(PDO::FETCH_ASSOC);

print_r($result);