<?php
require_once 'MagicMethod.php';
class a extends Developer
{
}
// khởi tạo 1 đối tượng (gọi method __construct)
$dev = new a(name: 'Nhật Anh', gender: "male");

$name = $dev->name;

echo $name . "\n";

$se = serialize($dev);

echo $se . "\n";

// $email = $dev->emmail;
// echo $email . "\n";

// $email2 =  $dev->email2;
// echo $email2 . "\n";

// // var_dump($dev);

// $dev->email2 = 'public123@gmail.com';

// $a = $dev->email2;

// echo $a . "\n";

// $dev->email = 'anh123@gmail.com';

// $a = $dev->email;

// echo $a . "\n";

// $test = new a();
// echo $test->email;
// // huỷ đối tượng (gọi __destruct)
// unset($dev);