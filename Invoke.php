<?php

require_once 'MagicMethod.php';

class Invoke extends Developer
{
}

$arr = [
    ['id' => 1, 'first_name' => 'John', 'last_name' => 'Doe'],
    ['id' => 3, 'first_name' => 'Alice', 'last_name' => 'Gustav'],
    ['id' => 2, 'first_name' => 'Bob', 'last_name' => 'Filipe']
];

usort($arr, new Invoke('first_name'));
print_r($arr);

usort($arr, new Invoke('last_name'));
print_r($arr);