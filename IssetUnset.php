<?php
class MagicMethod
{
    private $arr = [];

    public function __set($key, $value)
    {
        $this->arr[$key] = $value;
    }

    public function __isset($key)
    {
        return isset($this->arr[$key]);
    }
    public function __get($name)
    {
        echo "Getting '$name'\n";
        if (array_key_exists($name, $this->arr)) {
            return $this->arr[$name];
        }
    }
    // public function __unset($key)
    // {
    //     echo "Working: Unset $this->arr[$key] \n";
    //     unset($this->arr[$key]);
    // }
}

class IssetUnset extends MagicMethod
{
}

//
$a = new MagicMethod();

var_dump(isset($a->name));
$name = $a->name;
echo $name . "\n";

$a->name = "NhatAnh";
var_dump(isset($a->name));

$name = $a->name;
echo $name . "\n";