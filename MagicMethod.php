<?php
abstract class Developer
{
    private $name;
    private $gender;
    protected $email = 'anh@gmail.com';
    public $email2 = 'public@gmail.com';

    public function __construct($name = '', $gender = 'female')
    {
        $this->name = $name;
        $this->gender = $gender;
        echo "đây là phương thức __construct() có các thuộc tính name: $this->name và gender: $this->gender\n";
    }

    public function __destruct()
    {
        echo "đây là method __destruct() \n";
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function __get($key) //được gọi khi truy cập các thuộc tính không phải public( private và protected)
    {
        // return $this->getEmail();
        if (property_exists($this, $key)) {
            //tiến hành lấy giá trị
            return $this->$key;
        } else {
            echo ('Không tồn tại thuộc tính' . "\n");
        }
        // return ('Phương thức __get() được gọi' . "\n");
    }

    public function __set($key, $value) // được gọi khi set giá trị cho thuộc tính không phải public
    {
        if (property_exists($this, $key)) {
            //tiến hành lấy giá trị
            $this->$key = $value;
        } else {
            echo ('Không tồn tại thuộc tính' . "\n");
        }

        // echo ('Phương thức __set() được gọi' . "\n");
    }

    public function __isset($name)
    {
        return isset($this->$name);
    }

    public function __unset($name)
    {
        echo "working: Unset $name \n";
        unset($this->$name);
    }

    public function __toString() // ham nay cho phep dung doi tuong nhu 1 string
    {
        return $this->name;
    }

    public function __invoke($a = [], $b = []) // ham nay dung khi co su dung opject nhu 1 ham
    {
        return $a[$this->name] <=> $b[$this->name];
    }

    public function __sleep()
    {
        return ['name', 'gender', 'email'];
    }

    public function __serialize()
    {
        return ['name', 'gender', 'email'];
    }
}